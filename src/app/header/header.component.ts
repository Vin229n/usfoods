import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  
})
export class HeaderComponent implements OnInit {
  @Output() logoutClicked = new EventEmitter();
   loginStatus=false;
   foods = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];
  constructor(private router:Router,private activeR:ActivatedRoute) { }

  ngOnInit() {
           this.router.events.subscribe((event)=>{
             if(event instanceof NavigationEnd )
             {
               if(event.url.indexOf('home')>-1)
               {
                 this.loginStatus=true;
               }
               else {
                 this.loginStatus=false;
               }
             }
           })
  }
  
 
}
