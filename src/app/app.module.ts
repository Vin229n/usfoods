import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule }  from '@angular/platform-browser/animations';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MyOwnCustomMaterialModule }  from './material.module'
import { ProductListComponent } from './productlist-component/productlist.component';
import { CurrentOrderComponent } from './current-order/current-order.component';
import { CreateOrderDialogComponent } from './create-order-dialog/create-order-dialog.component';
import { JwPaginationComponent } from 'jw-angular-pagination';
import { ScrollingModule } from '@angular/cdk/scrolling';
import {DatePoDialogComponent} from './date-po-dialog/date-po-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
	  ProductListComponent,
	  CurrentOrderComponent,
    CreateOrderDialogComponent,
    JwPaginationComponent,
    DatePoDialogComponent
  ],
  entryComponents:[
    CreateOrderDialogComponent,
    DatePoDialogComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AngularFontAwesomeModule,
    MyOwnCustomMaterialModule,
    ScrollingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
