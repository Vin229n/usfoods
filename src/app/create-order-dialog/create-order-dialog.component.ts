import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as _ from 'lodash';
@Component({
  selector: 'app-create-order-dialog',
  templateUrl: './create-order-dialog.component.html',
  styleUrls: ['./create-order-dialog.component.scss']
})
export class CreateOrderDialogComponent implements OnInit {
  deliveryDate=new Date(new Date().getTime() + (4 * 24 * 60 * 60 * 1000));
   customerDetails:any;
   selectedOrderType:string="New";
   poNumber:string;
  constructor(
    public dialogRef: MatDialogRef<CreateOrderDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any) {}

    createOrder(flag): void {
    if(flag && this.selectedOrderType=="New"){
      console.log("Create Order"+this.deliveryDate);
      var orderObject=_.cloneDeep(this.customerDetails.orderList[0]);
       orderObject.deliveryDate=this.deliveryDate;
       orderObject.submitted=false;
       orderObject.po=this.poNumber;
       orderObject.orderDetails.updateEaches=0;
       orderObject.orderDetails.updateCases=0;
       orderObject.orderDetails.prodEntryList=[];
       orderObject.orderDetails.quickEntryList=[];

      this.dialogRef.close(orderObject);
    }else{
      this.dialogRef.close(null);
    }

  }
  ngOnInit() {
    this.customerDetails=this.data.customer;
  }

}
