import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-date-po-dialog',
  templateUrl: './date-po-dialog.component.html',
  styleUrls: ['./date-po-dialog.component.scss']
})
export class DatePoDialogComponent implements OnInit {
   type:boolean;
   orderObject:Date;
  constructor(
    public dialogRef: MatDialogRef<DatePoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any) {}

    createOrder(flag): void {
        this.dialogRef.close();
      
  
    }
 
    ngOnInit() {
    this.orderObject=this.data.orderObject;
    this.type=this.data.dialogType;
  }

}
