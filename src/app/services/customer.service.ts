import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';
@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(private http: HttpClient) { }

  getCustomerById(id) {
    let that = this;
    return new Promise((resolve, reject) => {
      this.http.get('assets/customerList.json')
        .subscribe((data:any) => {
          let customer=_.filter(data, function (value) {
            return value.customerId===id;
          })[0]
          resolve(customer);
        });
    })
}
}
