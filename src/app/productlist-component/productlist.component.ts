import { Component, OnInit, ViewChild, ElementRef, Renderer2, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';
import { CurrentOrderComponent } from '../current-order/current-order.component';
import { CreateOrderDialogComponent } from '../create-order-dialog/create-order-dialog.component';
import { MatDialog, MatInput } from '@angular/material';
import { ProductService } from '../services/product.service';
import { CustomerService } from '../services/customer.service';
import { Observable ,isObservable} from 'rxjs';
import { async } from 'q';
import './productList.component.scss'
@Component({
  selector: 'app-productList',
  templateUrl: './productList.component.html',
  styleUrls: ['./productlist.component.scss']
})
export class ProductListComponent implements OnInit {
  editProductNoteFlag=false;
  @Input() inputFromJSP = "";
  xpandStatus = true;
  sortOptions = [{
    type: "groupLine", name: "Custom Group(Line #)"
  }, {
    type: "groupAlpha", name: "Custom Group(Alpha)"
  }, {
    type: "alpha", name: "Alphabetical"
  }, {
    type: "foodClass", name: " US Food Class"
  }]
  // Mat Panel Expansion on Custom Button variables(Done By Kenny)
  toggleStatusDetailed: boolean = true;
  toggleStatusCompact: boolean = false;
  @Input() indexExpanded: number = -1;
  loadIndex: number = -1;
  // Mat Panel Expansion on Custom Button variables(Done By Kenny)
  customerDetails: any;
  expandedElement: any;
  displayList: any = [];
  searchText: String;
  
  enteredFirst = true;
  notFound = false;
  
  sortByStatus: string;
  delay = false;
  paginationList:any=[];
  pageSize: number = 100;
  dispPage = "100"
   viewKeys = [];
   loadFirst=true;

   //Object to store Keyboard click Key Values(Done By Kenny)
    KEYCODE = {
    LEFT: 37,
    RIGHT: 39,
    TAB : 9,
    UP: 38,
    DOWN : 40,
    ENTER :13,
    SHIFT : 16,
  };
  constructor(private http: HttpClient, private productService: ProductService, private dialog: MatDialog, private customerService: CustomerService, private renderer2: Renderer2) { }

  @ViewChild(CurrentOrderComponent) currentOrderComponent: CurrentOrderComponent;

  ngOnInit() {
    this.loadFirst = true;
    let that = this;
    console.log("Inside Product Component",this.inputFromJSP);
    this.customerService.getCustomerById("#113168").then(function (resp: any) {
      that.customerDetails = resp;
    });

    that.paginationList = that.productService.getProductList()
    that.productService.getProductList().then(() => {
      this.setViewKeys()
    })
    
  }


// --------------------Up/Down/Right/Left/Tab Focus Functionality Starts here(Done By Kenny) -------------------------
 
buttons : any = [];

// Recieve the Event from keyboard click on Input Element and take appropraite actions
  onKeyDown(event) {
    switch (event.keyCode) {
      case this.KEYCODE.RIGHT :
        console.log("RIGHT CALLED")
        event.preventDefault();
        this.focusNextItem();
        break;
      case this.KEYCODE.TAB:
        console.log("TAB-RIGHT-CALLED")
        event.preventDefault();
        this.focusNextItem();
        break;
      case this.KEYCODE.DOWN:
        console.log("Down -CALLED")
        event.preventDefault();
        this.focusNextItem();
        break;  
      case this.KEYCODE.ENTER:
        console.log("Enter -CALLED")
        event.preventDefault();
        this.focusNextItem();
        break;
      case this.KEYCODE.LEFT:
        event.preventDefault();
        console.log("Left -CALLED")
        this.focusPreviousItem();
        break;
      case this.KEYCODE.UP:
        event.preventDefault();
        console.log("Up -CALLED")
        this.focusPreviousItem();
        break;
      case (this.KEYCODE.SHIFT ):
        event.preventDefault();
        console.log("Shift-Tab -CALLED")
        this.focusPreviousItem();
        break;
    }
  }

  

   onClick(event) {
    // Make sure the clicked item is one of the buttons and
    // not something random :) impval is the class given to Input element Of 'CS'
    this.buttons = document.querySelectorAll('.inpval');
    
    // Select all buttons and set tabIndex to -1
    this.buttons.forEach(button =>{
        button.tabIndex =-1;
    })
    
    //Activate(Focus) only the Input Filed which is clicked
    this.activate(event.target);
  }

    
   focusNextItem() {
     let btn;
     try{
      this.buttons.forEach((button,i) => {
        // if(i != (this.buttons.length) ){
          //If Current Input field Is Active
          if(button.tabIndex == 0)
          {   
              //Set Current Input field tabIndex to -1         
              button.tabIndex =-1;
              //Select the Next Input field
              btn = this.buttons[i+1];                  
          }
        // }   
      })
     }
     catch(e)
     {
       console.log(e);
     }  
     
    //Activate(Focus) the selected Input Field
    this.activate(btn);
  }


  focusPreviousItem() {
    let btn;

    try{
      this.buttons.forEach((button,i) => {
        //Condition to Check First Index Of Input Box
        if(i!=0){
          if(button.tabIndex == 0)
          {
            //Decrement the Index by 1
            btn = this.buttons[i-1];
            //Set Current tabIndex to -1
            button.tabIndex = -1;
          }
        }
      })
    }
    catch(e){
      console.log(e);
    }
    //Activate the current selected input field 
    this.activate(btn);
  }

  activate(item) {
    try{
      //Activated Input fields will be set to tabIndex=0
      item.tabIndex = 0;
      item.focus();
    }
    catch(e)
    {
      // alert("Index Boundary Reached");
    }
    
  }

// -----------------------Up/Down/Right/Left/Tab Focus Functionality Ends Here (Done By Kenny) -------------------
 
// -------------------  Mat Panel Expansion on Custom Button Click Starts Here(Done By Kenny) ------------------

   togglePanels(event:Event,index: number) {
     event.preventDefault();
     event.stopImmediatePropagation();
     event.stopPropagation();
     this.indexExpanded = index == this.indexExpanded ? -1 : index;
    this.loadIndex = index == this.loadIndex ? -1 : index;
  }
// -------------------  Mat Panel Expansion on Custom Button Click Ends Starts Here(Done By Kenny) ------------------

// -------------------  Detailed & Compact View Functionality Starts Here(Done By Kenny) ------------------
  setToggleStatusDetailed() {
    this.toggleStatusDetailed = true;
    this.toggleStatusCompact = false;
  }

  setToggleStatusCompact() {
    this.toggleStatusCompact = true;
    this.toggleStatusDetailed = false;
  }

// -------------------  Detailed & Compact View Functionality Ends Here(Done By Kenny) ------------------



  openDialog(): Observable<any> {
    const dialogRef = this.dialog.open(CreateOrderDialogComponent, {
      width: '450px',
      data: {
        customer: this.customerDetails
      }
    });

    return dialogRef.afterClosed();
  }
  enteredQuantity(value: number, data, type: string) {
    //console.log(value, data,type);
    if (this.enteredFirst && value) {
      this.openDialog().subscribe((order) => {
        if (order) {
          this.enteredFirst = false;
          this.updateCurrentOrder(value, data, type, order);
        }

      });

    }
    else if(value) {
      this.updateCurrentOrder(value, data, type, null);
    }

  }
  updateCurrentOrder(value, data, type, order) {
    data[type] = value;
    var clonedData = _.clone(data);
    this.currentOrderComponent.updateCurrentOrderFromProduct(clonedData, type, order);
  }


  //Vinayak and Sujay Code Changes ----------------------------------------------------------------------
  search(clearFlag) {
    this.loadFirst =false;
    this.notFound = false;
    this.productService.copyList = _.clone(this.productService.unfilteredList);
    if (!clearFlag && this.searchText) {
      this.productService.copyList = this.productService.copyList.filter((arrItem) => {
        let descriptionArray = arrItem.description.split(/[ ,-]+/).map(function (x) { return x.toUpperCase() });
        let brandArray = arrItem.brand.split(/[ ,]+/).map(function (x) { return x.toUpperCase() });
        if (descriptionArray.includes(this.searchText.toUpperCase())) {
          return arrItem;
        } else if (brandArray.includes(this.searchText.toUpperCase())) {
          return arrItem;
        } else if (arrItem.quantity.indexOf(this.searchText) >= 0) {
          return arrItem;
        } else if (arrItem.priceCases.indexOf(this.searchText) >= 0) {
          return arrItem;
        } else if (arrItem.id.indexOf(this.searchText) >= 0) {
          return arrItem;
        }
      })

      if (this.productService.copyList.length == 0) {
        this.notFound = true;
      }
      else {
        this.notFound = false;
      }
      this.paginationList = _.clone(this.productService.copyList);
    }
    else {
      this.notFound = false;
      this.paginationList = _.clone(this.productService.unfilteredList);
      this.sortByStatus = undefined;
    }
    this.setViewKeys()
  }

  sort(event) {
    this.loadFirst =false;
    if (event.value == 'groupLine') {
      this.sortByType('lineNo')
    } else if (event.value == 'groupAlpha') {
      this.sortByType('groupName')
    } else if (event.value == "alpha") {
      this.sortByStatus = 'alpha'
      this.sortByType('description')
    } else if (event.value == 'foodClass') {
      this.sortByStatus = 'foodClass'
      this.sortByType('foodClass')
    }

  }

  sortByType(type) {
    this.productService.copyList.sort(function (a, b) { return a[type] > b[type] ? 1 : -1 });
    this.paginationList = _.clone(this.productService.copyList)
    this.setViewKeys();
  }

  setViewKeys() {
    let that = this;
    let group = this.sortByStatus === 'foodClass' ? 'foodClass' : 'groupName'
    let groupedArray = _.groupBy(that.productService.copyList, function (g) {
      return g[group]
    });
    this.viewKeys = Object.keys(groupedArray);
    this.viewKeys.sort();
  }

  view(event) {
    this.loadFirst =false;
    if (event.value !== 'All') {
      let tempArrayToView = _.clone(this.productService.copyList);
      tempArrayToView = tempArrayToView.filter((temp) => {
        if (this.sortByStatus === 'foodClass')
          return temp.foodClass === event.value;
        else
          return temp.groupName === event.value;
      })
      this.paginationList = _.clone(tempArrayToView);
    }
    else {
      this.paginationList = _.clone(this.productService.copyList);
    }
    

  }

  onChangePage(pageOfItems: Array<any>) {
   
    let group = this.sortByStatus === 'foodClass' ? 'foodClass' : 'groupName'
    this.displayList = _.groupBy(pageOfItems, function (g) {
      return g[group];
    })
    
  }

  isItObservable(){
    if(isObservable(this.paginationList)){
      return true;
    }
    return false;
  }

  changePageSize(e) {
    this.loadFirst = false;
    this.pageSize = parseInt(e.value);
    this.paginationList = _.clone(this.productService.copyList);
  }
  //end-------------------------------------------------------------------------------------------------

 

  //Changes made by Ajay
  // Restriction for a positive number directive
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 49 || charCode > 57)) {
      return false;
    }
    return true;

  }


}